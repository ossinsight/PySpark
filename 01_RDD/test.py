
from pyspark.sql import *

class MyClass:
    def __init__(self,name='aa'):
        pass  # 构造方法不需要任何初始化操作
    def func(self):
        print("函数")


if __name__ == '__main__':

    spark=SparkSession.builder.getOrCreate()
    sc=spark.sparkContext

    # 类对象
    my_instance1 = MyClass
    # 类实例
    my_instance2 = MyClass()


    print(my_instance1)
    print(my_instance2)
    print(my_instance2.func())
    # func() missing 1 required positional argument: 'self'
    print(my_instance1.func())

