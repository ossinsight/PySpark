
# from . import connections  # noqa: E402
from pymysql import Connection

conn=Connection(
    host="hadoop3-02",
    port=3306,
    user="root",
    password="000000"
    # ,autocommit=True
)
print(conn.get_server_info())
conn.select_db("test1_pymysql")
cursor=conn.cursor()

'''cursor.execute("""CREATE TABLE student(id int,name varchar(20), age int,gender varchar(10))
                    engine=innodb
                    default charset=utf8;""")
cursor.execute("""insert into student values 
                (1,'周杰伦',66,'男'),
                (2,'林俊杰',66,'男'),
                (3,'邓紫棋',99,'女');""")'''
cursor.execute("""select * from test1_pymysql.student;""")
result=cursor.fetchall()
print(type(result))
print(result)
for row in result:
    print(row)

# update和insert into 需要提交commit事务
cursor.execute("""UPDATE student SET age=66
                    WHERE id=1;""")
conn.commit()
conn.close()

