#coding:utf8

from pyspark.sql import *
import os
from pyspark.sql.types import *
import pandas as pd

if __name__ == '__main__':
    spark=SparkSession.builder.appName("test1_dataFrame_create")\
        .master("local[*]").getOrCreate()
    sc=spark.sparkContext
    localhost_path = 'file://' + os.getcwd() + '/../data/input/sql/people.txt'
    # 构建StructType, text数据源, 读取数据的特点是, 将一整行只作为`一个列`读取, 默认列名是value 类型是String
    schema=StructType().add("data",StringType(),True)
    df=spark.read.format('text').schema(schema)\
        .load(localhost_path)
    df.printSchema()
    df.show()























































