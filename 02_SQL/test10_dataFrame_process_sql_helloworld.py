#coding:utf8

from pyspark.sql import *
import os
from pyspark.sql.types import *
import pandas as pd

if __name__ == '__main__':
    spark=SparkSession.builder.appName("test1_dataFrame_create")\
        .master("local[*]").getOrCreate()
    sc=spark.sparkContext
    localhost_path = 'file://' + os.getcwd() + '/../data/input/sql/stu_score.txt'
    df=spark.read.csv(localhost_path,"id INT,subject string,score int")
    # df.printSchema()
    # df.show()

    df.createTempView("score1")
    # 全局临时试图在使用的时候需要在前面带上global_temp.
    df.createGlobalTempView("score2")
    df.createOrReplaceTempView("score3")
    spark.sql("""
    select subject,count(1) from score1 group by subject
    """).show()

    spark.sql("""
    select subject,count(*) from global_temp.score2 group by subject
    """).show()

    spark.sql("""
    select subject,count(1) from score3 group by subject
    """).show()























































