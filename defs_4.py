# coding:utf8
import json

def city_with_category(data: dict) -> str:
    return data['areaName'] + "_" + data['category']


if __name__ == '__main__':
    print(json.loads('{"name": "John", "age": 30, "city": "New York"}'))
    print(type(json.loads('{"name": "John", "age": 30, "city": "New York"}')))
    print(type(json.loads('{"key1":1,"key2":2}')))
    print(type(json.loads('[{"key1":1,"key2":2}]')))

    list1 = [{"key1": 1, "key2": 2}]
    dict1 = {"key1": 1, "key2": 2}
    print(type(json.dumps(list1)))
    print(type(json.dumps(dict1)))
