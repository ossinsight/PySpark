#coding:utf8

from pyspark.sql import *
import os
from pyspark.sql.types import *


if __name__ == '__main__':
    spark=SparkSession.builder.appName("test1_dataFrame_create")\
        .master("local[*]").getOrCreate()
    sc=spark.sparkContext
    localhost_path = 'file://' + os.getcwd() + '/../data/input/sql/people.txt'
    rdd1=sc.textFile(localhost_path)
    rdd2=rdd1.map(lambda line:line.split(','))\
        .map(lambda lt:(lt[0],int(lt[1])))

    schema=StructType().add("name",StringType(),True)\
        .add("age",IntegerType(),False)
    # StructType(List(StructField(name,StringType,true),StructField(age,IntegerType,false)))
    print(schema)

    df=spark.createDataFrame(rdd2,schema)
    df.printSchema()
    df.show()
























































