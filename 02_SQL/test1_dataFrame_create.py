#coding:utf8

from pyspark.sql import *
import os

if __name__ == '__main__':
    spark=SparkSession.builder.appName("test1_dataFrame_create")\
        .master("local[*]").getOrCreate()
    sc=spark.sparkContext
    localhost_path = 'file://' + os.getcwd() + '/../data/input/sql/people.txt'
    rdd1=sc.textFile(localhost_path)
    rdd2=rdd1.map(lambda line:line.split(','))\
        .map(lambda lt:(lt[0],int(lt[1])))

    df=spark.createDataFrame(rdd2,['name','age'])
    df.printSchema()
    """
    打印df中的数据
    参数1 表示打印出来多少条数据，默认20
    参数2 表示是否对列进行截断，如果列的数据长度超过20个字符串长度，后续内容不显示以...代替
    如果给False 表示不截断全部显示，默认是True
    """
    df.show(20,False)
    df.createOrReplaceTempView("people")
    spark.sql("select * from people where age < 30").show()

























































