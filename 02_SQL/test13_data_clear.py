#coding:utf8
import time

from pyspark.sql import *
import os
from pyspark.sql.types import *
import pandas as pd
import pyspark.sql.functions as f
from pyspark import *

if __name__ == '__main__':
    spark=SparkSession.builder.appName("test11_wordcount_demo")\
        .master("local[*]")\
        .config("spark.sql.shuffle.partitions",2) \
        .getOrCreate()
    """
    spark.sql.shuffle.partitions 参数指的是，在sql计算中，shuffle算子阶段默认的分区数是200个
    对于集群模式来说，200也不算多
    这个参数和spark rdd中设置并行度的参数是相互独立的
    """
    sc=spark.sparkContext
    localhost_path ="file://"+os.getcwd()+"/../data/input/sql/people.csv"
    # localhost_path =os.getcwd()+"/../data/input/sql/u.data"
    df=spark.read.csv(localhost_path,header=True,sep=";")
    # todo 数据去重，不传入任何参数，全列组合整体去重
    df.dropDuplicates().show()
    df.dropDuplicates(['age','job']).show()

    # todo 缺失值处理 ，不传入参数，只要列中又null就删除一行数据
    df.dropna().show()
    # 删除少于 thresh 个有效值的行，当thresh传入时，被覆盖 = 至少有三个有效值
    df.dropna(thresh=3).show()
    # thresh 在subset列中生效
    df.dropna(thresh=2,subset=['age','name']).show()

    # 填充缺失值
    df.fillna("loss").show()
    df.fillna("空",['job']).show()
    df.fillna({"name":"未知姓名","age":18,"job":"worker"}).show()




