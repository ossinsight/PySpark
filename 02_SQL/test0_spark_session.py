import os

from pyspark.sql import *
from pyspark.sql.types import *

if __name__ == '__main__':
    spark=SparkSession.builder.appName("test0_spark_session").master("local[*]").getOrCreate()
    # 这里调用了属性而不是方法，如果加括号会报错
    sc=spark.sparkContext
    # a=spark.getActiveSession() test
    # b=SparkSession.getActiveSession() test
    print(os.getcwd())
    localhost_path = 'file://' + os.getcwd() + '/../data/input/sql/stu_score.txt'
    df=spark.read.csv(localhost_path,sep=',',header=False)
    # shema=StructType().add("name",StringType,True)    test
    df2=df.toDF("id","name","score")
    df2.printSchema()
    df2.show()

    df2.createOrReplaceTempView("score")

    spark.sql(
        """
        select * from score where name = '语文' limit 5
        """
    ).show()

    df2.where("name='语文'").limit(5).show()























