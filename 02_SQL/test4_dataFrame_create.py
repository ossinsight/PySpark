#coding:utf8

from pyspark.sql import *
import os
from pyspark.sql.types import *
import pandas as pd

if __name__ == '__main__':
    spark=SparkSession.builder.appName("test1_dataFrame_create")\
        .master("local[*]").getOrCreate()
    sc=spark.sparkContext
    localhost_path = 'file://' + os.getcwd() + '/../data/input/sql/people.txt'
    pdf=pd.DataFrame({
        "id":[1,2,3],
        "name":["张大仙","王晓晓","吕不为"],
        "age":[11,21,11]
    })
    schema=StructType().add("id",IntegerType(),True).add("name",StringType(),True).add("age",IntegerType(),True)
    df=spark.createDataFrame(pdf,schema)
    df.printSchema()
    df.show()
























































