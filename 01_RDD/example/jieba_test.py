
import jieba

if __name__ == '__main__':
    content = '小明硕士毕业于中国科学院计算所，后在清华大学深造'
    """ 
    cut_all：模型类型。True
    表示完整模式，False
    表示准确模式。
    """

    # 允许二次组合
    result1=jieba.cut(content,True)
    print(result1)
    print(type(result1))
    print(list(result1))

    # 不允许二次组合
    result2=jieba.cut(content,False)
    print(result2)
    print(type(result2))
    print(list(result2))

#     搜索引擎模式
    result3=jieba.cut_for_search(content)
    print(",".join(result3))
