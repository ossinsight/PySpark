from typing import Optional


class MyClass:
    builder: int =100
    def my_method(self):
        print("这是一个方法")

my_instance = MyClass()
my_instance1 = MyClass
print(my_instance)
print(my_instance1)
a=my_instance.my_method()  # 方法调用
b=my_instance.my_method  # 方法引用
print(a)
print(b)
print("=================")
def my_method():
    print("这是一个没有参数的方法")

my_method  # 正确的方式，不需要括号    备注：但这不是方法的调用
my_method()  # 正确的方式，但不是必需的
print("@@@@@@@@@@@@@@@@")
print(MyClass().builder)
print(MyClass.builder)





